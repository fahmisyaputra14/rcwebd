import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')
WebUI.navigateToUrl('https://rc-webm.rctiplus.com/')
WebUI.maximizeWindow()
Login()
Play_Episode()
Play_Clip()
WebUI.closeBrowser()


String Login() 
{
	
	WebUI.click(findTestObject('login_asset/Page_RCTI - Satu Aplikasi, Semua Hiburan/i_EXPLORE_fas fa-bars'))
	WebUI.setText(findTestObject('login_asset/Page_Login Akun - RCTI/input_Email or Phone Number_username'),
		'fahmisyaputra14@yahoo.com')
	WebUI.setEncryptedText(findTestObject('login_asset/Page_Login Akun - RCTI/input_Password_password'), 'RigbBhfdqOBDK95asqKeHw==')
	WebUI.click(findTestObject('login_asset/Page_Login Akun - RCTI/button_Log In'))
	WebUI.delay(5)//test tambah komentar di git
	
		
}

String Play_Episode() 
{

	WebUI.click(findTestObject('Play_episode_Asset/Page_RCTI - Satu Aplikasi, Semua Hiburan/Ikatan_Cinta'))
	WebUI.click(findTestObject('Play_episode_Asset/Page_Nonton Streaming Ikatan Cinta Online Sub Indo - RCTI/Episodes'))
	WebUI.click(findTestObject('Play_episode_Asset/Page_Nonton Streaming Ikatan Cinta Online Download Full Episode Sub Indo - RCTI/Ikatan_Cinta_Eps_324'))
	
	if (WebUI.waitForElementVisible(findTestObject('Play_episode_Asset/Page_Nonton Streaming Ikatan Cinta Eps. 324 - Tidak Ada Kata Maaf Bagi Elsa Online Download Full Episode Sub Indo - RCTI/btnLewati_Iklan'),1))
		{
			WebUI.click(findTestObject('Play_episode_Asset/Page_Nonton Streaming Ikatan Cinta Eps. 324 - Tidak Ada Kata Maaf Bagi Elsa Online Download Full Episode Sub Indo - RCTI/btnLewati_Iklan'))
		}
	
//	WebUI.delay(20)
	WebUI.click(findTestObject('Player_Asset/Page_Nonton Streaming Ikatan Cinta Eps. 324 - Tidak Ada Kata Maaf Bagi Elsa Online Download Full Episode Sub Indo - RCTI/PlayPauseButton'))
	WebUI.delay(7)
	WebUI.click(findTestObject('Play_episode_Asset/Page_Nonton Streaming Ikatan Cinta Eps. 324 - Tidak Ada Kata Maaf Bagi Elsa Online Download Full Episode Sub Indo - RCTI/Layar_Penuh'))
	WebUI.delay(5)	
	WebUI.click(findTestObject('Play_episode_Asset/Page_Nonton Streaming Ikatan Cinta Eps. 324 - Tidak Ada Kata Maaf Bagi Elsa Online Download Full Episode Sub Indo - RCTI/Layar_Penuh'))
	WebUI.delay(5)
	WebUI.click(findTestObject('Player_Asset/Page_Nonton Streaming Ikatan Cinta Eps. 324 - Tidak Ada Kata Maaf Bagi Elsa Online Download Full Episode Sub Indo - RCTI/fastforwardButton'))
	WebUI.delay(5)
	WebUI.click(findTestObject('Player_Asset/Page_Nonton Streaming Ikatan Cinta Eps. 324 - Tidak Ada Kata Maaf Bagi Elsa Online Download Full Episode Sub Indo - RCTI/backforward_button'))
	WebUI.delay(5)
	
	
}

String Play_Extras()
{
	
	WebUI.click(findTestObject('Play_Extra_Assset/Page_Nonton Streaming Ikatan Cinta Eps. 324 - Tidak Ada Kata Maaf Bagi Elsa Online Download Full Episode Sub Indo - RCTI/Extras'))
	WebUI.delay(5)
	WebUI.click(findTestObject('Play_Extra_Assset/Page_Nonton Video Extra Ikatan Cinta Sub Indo - RCTI/HebohKarena_KataDariKiki'))
	WebUI.delay(10)
	WebUI.click(findTestObject('Play_Extra_Assset/Page_Nonton Streaming Heboh Karena 1 Kata Dari Kiki Sub indo - RCTI/PlayPauseExtra'))
	WebUI.delay(3)
	WebUI.click(findTestObject('Play_Extra_Assset/Page_Nonton Streaming Heboh Karena 1 Kata Dari Kiki Sub indo - RCTI/PlayPauseExtra'))
	WebUI.delay(5)
	WebUI.click(findTestObject('Play_Extra_Assset/Page_Nonton Streaming Heboh Karena 1 Kata Dari Kiki Sub indo - RCTI/LayarPenuh_Extra'))
	WebUI.delay(4)
	
}

String Play_Clip()
{
//	String klikPancingan = 'Play_Clip_Asset/Page_Nonton Streaming Hancur Lebur Hidup Elsa - Ikatan Cinta Eps. 322-323 Sub Indo - RCTI/Player_clip';
	
	
	WebUI.click(findTestObject('Play_Clip_Asset/Page_Nonton Streaming Ikatan Cinta Online Sub Indo - RCTI/Clips'))
	WebUI.click(findTestObject('Play_Clip_Asset/Page_Tonton Video Menarik dan Lucu Ikatan Cinta Sub Indo - RCTI/Hancur_Lebur_Hidup_Elsa'))
	WebUI.delay(10)
	
	WebUI.mouseOver(findTestObject('Play_Clip_Asset/Page_Nonton Streaming Hancur Lebur Hidup Elsa - Ikatan Cinta Eps. 322-323 Sub Indo - RCTI/Player_clip'))
	
	WebUI.click(findTestObject('Play_Clip_Asset/Page_Nonton Streaming Hancur Lebur Hidup Elsa - Ikatan Cinta Eps. 322-323 Sub Indo - RCTI/PlayPause'))
	WebUI.delay(5)

	
	WebUI.click(findTestObject('Play_Clip_Asset/Page_Nonton Streaming Hancur Lebur Hidup Elsa - Ikatan Cinta Eps. 322-323 Sub Indo - RCTI/Layar_Penuh'))
	WebUI.delay(5)
	
	WebUI.click(findTestObject('Play_Clip_Asset/Page_Nonton Streaming Hancur Lebur Hidup Elsa - Ikatan Cinta Eps. 322-323 Sub Indo - RCTI/PlayPause'))
	WebUI.doubleClick(findTestObject('Play_Clip_Asset/Page_Nonton Streaming Hancur Lebur Hidup Elsa - Ikatan Cinta Eps. 322-323 Sub Indo - RCTI/fastForward_Clip'))
	
	WebUI.mouseOver(findTestObject('Play_Clip_Asset/Page_Nonton Streaming Hancur Lebur Hidup Elsa - Ikatan Cinta Eps. 322-323 Sub Indo - RCTI/Player_clip'))
	WebUI.click(findTestObject('Play_Clip_Asset/Page_Nonton Streaming Hancur Lebur Hidup Elsa - Ikatan Cinta Eps. 322-323 Sub Indo - RCTI/backForward_Clip'))
//	WebUI.delay(5)
	
}