<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Test Login</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>e59abd95-a827-45f8-9d4a-18cb4ad89615</testSuiteGuid>
   <testCaseLink>
      <guid>744c3356-f803-45b4-b08c-ce6cedace27e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>10c01b90-38bb-4e29-8127-3f791f3a671a</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DF_Login</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>10c01b90-38bb-4e29-8127-3f791f3a671a</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Ùsername</value>
         <variableId>c1bcff30-fafb-4575-a4f2-7e59d57ec61b</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>10c01b90-38bb-4e29-8127-3f791f3a671a</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>60a40b86-fa90-4fb1-9178-b7123223fee8</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
