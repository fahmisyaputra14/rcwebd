<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Ikatan_Cinta</name>
   <tag></tag>
   <elementGuidId>84fdba25-050b-422e-aaef-493c309bb399</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;line-up-square-image-ctn&quot;)/div[@class=&quot;col-md-4 col-sm-6 item&quot;]/div[@class=&quot;box19&quot;]/div[@class=&quot;box-content&quot;]/ul[@class=&quot;subtitle&quot;]/li[1]/a[1]/h5[1][count(. | //*[(text() = 'Ikatan Cinta' or . = 'Ikatan Cinta')]) = count(//*[(text() = 'Ikatan Cinta' or . = 'Ikatan Cinta')])]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#line-up-square-image-ctn > div.col-md-4.col-sm-6.item > div.box19 > div.box-content > ul.subtitle > li > a > h5</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='line-up-square-image-ctn']/div/div/div/ul/li/a/h5</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h5</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Ikatan Cinta</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;line-up-square-image-ctn&quot;)/div[@class=&quot;col-md-4 col-sm-6 item&quot;]/div[@class=&quot;box19&quot;]/div[@class=&quot;box-content&quot;]/ul[@class=&quot;subtitle&quot;]/li[1]/a[1]/h5[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='line-up-square-image-ctn']/div/div/div/ul/li/a/h5</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Line Up Square Image'])[1]/following::h5[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add to My List'])[8]/following::h5[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add to My List'])[9]/preceding::h5[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='MasterChef Indonesia Season 8'])[1]/preceding::h5[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Ikatan Cinta']/parent::*</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section[10]/div/div/div/div[2]/div/div/div/ul/li/a/h5</value>
   </webElementXpaths>
</WebElementEntity>
